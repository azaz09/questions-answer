﻿CREATE DATABASE qanswer;
CREATE TABLE qanswer.users
 (
   id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    users TEXT NOT NULL,
    email TEXT NOT NULL,
   pass_hash TEXT,
   active BIT
 )