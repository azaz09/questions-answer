<?php
session_start();
    require_once 'connect/db_connect.php';
    function searchFor($database, $u, $p) {

    $query = $database->prepare('SELECT users AND pass_hash FROM users WHERE users=:login OR pass_hash=:pass');
    $query->execute([":login" => $u, ":pass" => $p]);
    $check = $query->rowCount();
    
    if ($check == 2) {
          $_SESSION['unCaught'] = "Coś poszło nie tak. Spróbuj zalogować się ponownie za chwilę.";
            header("Location: main.php");
    }

    if ($check == 1) {
        header("Location: game.php");
        $_SESSION["inGame"]= 1;
    }
    
            
}

if (isset($_POST['user'])&&isset($_POST['pass'])) {

    $user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_SPECIAL_CHARS);
    $pass = password_hash($_POST['pass'], PASSWORD_DEFAULT);
    searchFor($db, $user, $pass);

    
    
} else {
    $_SESSION['unCaught'] = "Nie podano loginu lub hasła";
header("Location: main.php");
}

