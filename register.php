<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <title>QA - Rejestracja</title>
    <meta name="description" content="opis od 140 do 170 - check in google">
    <meta name="keywords" content="słowa, kluczowe">
        <?php
        require ('common/head.php');
        $start = session_start();
        ?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid">
            <section class="my-5">
            <div class="row">
                <div class=" mx-auto py-3 col-lg-12 col-md-12 col-sm-12 bg-light text-center text-dark mt-2 mb-2 border border-info rounded" style="max-width: 517.83px;">
                    <h2>Rejestracja</h2>
                </div> 

            </div>
            <div class="mx-auto bg-light my-auto border border-info rounded" style="width: 517.83px;">
                <form class="my-4 mx-4" method="POST" action="save.php">
                    <div class="form-group">

                        <label for="user-name">Nazwa użytkownika:</label>
                        <input type="text" class="form-control" id="uname" name="uname" aria-describedby="uHelp" placeholder="podaj nazwę użytkownika" value="<?=isset($_SESSION['checked1']) ? $_SESSION['checked1'] : '';?>">
                        <small id="uHelp" class="form-text text-danger">
                            <?php
                            if (isset($_SESSION['uname'])) {

                                echo ($_SESSION['uname']);
                                unset($_SESSION['uname']);
                            }
                            ?>
                        </small>

                    </div>
                    <div class="form-group">
                        <label for="e-mail">E-mail:</label>
                        <input type="email" class="form-control" id="e-mail" name="email" aria-describedby="emailHelp" placeholder="wprowadź adres e-mail" value="<?=isset($_SESSION['checked2']) ? $_SESSION['checked2'] : '';?>">
                        <small id="eHelp" class="form-text text-danger">
<?php
if (isset($_SESSION['email'])) {
    echo $_SESSION['email'];
    unset($_SESSION['email']);
}
?>
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="pass">Hasło:</label>
                        <input type="password" class="form-control" id="Fpass" name="Fpass" placeholder="hasło:">
                        <small id="pHelp" class="form-text text-danger">
<?php
if (isset($_SESSION['pass'])) {
    echo $_SESSION['pass'];
    unset($_SESSION['pass']);
}
?>
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="pass1">Potwierdź hasło:</label>
                        <input type="password" class="form-control" id="Spass" name="Spass" placeholder="Potwierdź hasło">
                        <small id="p1Help" class="form-text text-danger">
<?php
if (isset($_SESSION['Spass'])) {
    echo $_SESSION['Spass'];
    unset($_SESSION['Spass']);
}
?>
                        </small>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="set" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Zgadzam się z <a href="reg.php">regulaminem</a></label>
                    </div>
                    <button type="submit" name="clicked" class="btn btn-primary my-2">Rejestruj</button><br/>
                    <span class="my-5"><a href="main.php">Powrót do logowania</a></span>
                </form>
            </div>
                </section>

                    </div>

<?php require_once 'common/end.php'; ?>
                    </body>
                    </html>
