<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <?php require 'common/head.php'?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid px-5 py-5 my-5">
                <section class="mx-auto bg-light my-auto border border-info rounded" style="width: 517.83px; height: 320px;">
                    <form class="my-auto mx-auto px-3 py-5">
                        <div class="form-group">
                            <label for="e-mail">E-mail:</label>
                            <input type="email" class="form-control" id="e-mail" aria-describedby="emailHelp" placeholder="wprowadź email">
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="pass">Hasło:</label>
                            <input type="password" class="form-control" id="pass" placeholder="hasło:">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Zgadzam się z <a href="reg.php">regulaminem</a></label>
                </div>
  <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            <section>
       
        </div>
        
        <?php require_once 'common/end.php';?>
    </body>
</html>
