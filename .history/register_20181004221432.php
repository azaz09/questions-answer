<?php
    //START REGISTER
    $uname = $_POST['uname'];
    $email = $_POST['e-mail'];
    $Fpass = $_POST['Fpass'];
    $Spass = $_POST['Spass'];
    
        if(!(isset($uname))) {
            $uname = "podaj nazwę użytkownika!";
        }


?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <?php require 'common/head.php'?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid">

                <div class="row">
                    <div class=" mx-auto py-3 col-lg-12 col-md-12 col-sm-12 bg-light text-center text-dark mt-5 mb-2 border border-info rounded" style="max-width: 517.83px;">
                        <h2>Rejestracja</h2>
                    </div> 
                
                </div>
                <section class="mx-auto bg-light my-auto border border-info rounded" style="width: 517.83px;">
                    <form class="my-4 mx-4" method="POST" action="">
                        <div class="form-group">
                            <label for="user-name">Nazwa użytkownika:</label>
                            <input type="text" class="form-control" id="uname" aria-describedby="uHelp" placeholder="podaj nazwę użytkownika">
                            <small id="uHelp" class="form-text text-danger"><?=$uname;?></small>
                        </div>
                        <div class="form-group">
                            <label for="e-mail">E-mail:</label>
                            <input type="email" class="form-control" id="e-mail" aria-describedby="emailHelp" placeholder="wprowadź email">
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="pass">Hasło:</label>
                            <input type="password" class="form-control" id="Fpass" placeholder="hasło:">
                        </div>
                        <div class="form-group">
                            <label for="pass1">Potwierdź hasło:</label>
                            <input type="password" class="form-control" id="Spass" placeholder="hasło:">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Zgadzam się z <a href="reg.php">regulaminem</a></label>
                </div>
  <button type="submit" class="btn btn-primary my-2">Rejestruj</button><br/>
  <span class="my-5"><a href="main.php">Powrót do logowania</a></span>
                    </form>
            <section>
       
        </div>
        
        <?php require_once 'common/end.php';?>
    </body>
</html>
