﻿CREATE DATABASE qaswer;
CREATE TABLE qaswer.users
 (
   id INT NOT NULL PRIMARY KEY,
    users CHAR(32) NOT NULL,
    email CHAR(120) NOT NULL,
   pass_hash VARCHAR(512),
   active BIT
 )