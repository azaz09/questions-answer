﻿CREATE DATABASE qanswer;
CREATE TABLE qanswer.users
 (
   id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    users STRING NOT NULL,
    email STRING NOT NULL,
   pass_hash STRING,
   active BIT
 )