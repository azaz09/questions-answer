<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <?php require 'common/head.php'?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid">

                <div class="row">
                    <div class=" mx-auto py-5 col-lg-12 col-md-12 col-sm-12 bg-light text-center text-dark mt-5 mb-2 border border-info rounded" style="max-width: 517.83px;">
                        <h1>Q&A</h1>
                    </div> 
                
                </div>
                <section class="mx-auto bg-light my-auto border border-info rounded" style="width: 517.83px; height: 320px;">
                    <form class="my-4 mx-4" method="POST" action="login.php">
                        <div class="form-group">
                            <label for="e-mail">E-mail:</label>
                            <input type="email" class="form-control" id="e-mail" aria-describedby="emailHelp" placeholder="wprowadź email">
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="pass">Hasło:</label>
                            <input type="password" class="form-control" id="pass" placeholder="hasło:">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Zgadzam się z <a href="reg.php">regulaminem</a></label>
                </div>
  <button type="submit" class="btn btn-primary my-2">Zaloguj</button><br/>
  <span class="my-5">Nie masz konta? Możesz założyć je<a href="register.php"> tutaj</a></span>
                    </form>
            <section>
       
        </div>
        
        <?php require_once 'common/end.php';?>
    </body>
</html>
