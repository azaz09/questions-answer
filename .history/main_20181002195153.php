<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <?php require 'common/head.php'?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid px-5 py-5 my-5">
                    <form class="mx-auto bg-light my-auto border border-info px-4 py-4" style="width: 45%;">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
  <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
        
       
        </div>
        
        <?php require_once 'common/end.php';?>
    </body>
</html>
