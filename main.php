<?php

   // require_once 'connect/db_connect.php';
session_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>QA - Login</title>
    <meta name="description" content="opis od 140 do 170 - check in google">
    <meta name="keywords" content="słowa, kluczowe">
        <?php require 'common/head.php'?>
    </head>
    <body class="bg-dark">
        <div class="container-fluid">

                <div class="row">
                    <div class=" mx-auto py-3 col-lg-12 col-md-12 col-sm-12 bg-light text-center text-dark mt-5 mb-2 border border-info rounded" style="max-width: 517.83px;">
                        <h1>Q&A</h1>
                    </div> 
                
                </div>
                <section class="mx-auto bg-light my-auto border border-info rounded" style="width: 517.83px; height: 320px;">
                    <form class="my-4 mx-4" method="POST" action="login.php">
                        <div class="form-group">
                            <label for="login">Login:</label>
                            <input type="text" class="form-control" id="user" name="user" aria-describedby="userHelp" placeholder="podaj login">
                        </div>
                        <div class="form-group">
                            <label for="pass">Hasło:</label>
                            <input type="password" class="form-control" id="pass" name="pass" placeholder="hasło:">
                            <small id="userHelp" class="form-text text-info"><u>
                                 <?php
                            if (isset($_SESSION['unCaught'])) {

                                echo ($_SESSION['unCaught']);
                                unset($_SESSION['unCaught']);
                            }
                            ?></u>
                            </small>
                        </div>
                      
  <button type="submit" class="btn btn-primary my-2">Zaloguj</button><br/>
  <span class="my-5">Nie masz konta? Możesz założyć je <a href="register.php">tutaj</a></span>
                    </form>
            <section>
       
        </div>
        
        <?php require_once 'common/end.php';?>
    </body>
</html>
