<?php 
session_start();
if(!isset($_SESSION["inGame"])) {
    header("Location: main.php");
}

?>
<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <title>QA - Gra</title>
        <meta name="description" content="opis od 140 do 170 - check in google">
        <meta name="keywords" content="słowa, kluczowe">
        <?php require ("common/head.php"); ?>
    </head>
    <body class="bg-dark bg-selected">
        <div class="container-fluid py-5">
            <div class="row bg-light box mx-auto my-5 align-middle border-info border border-rounded">
                <div class="text-muted col-xl-4 px-3 py-3 col-lg-4 col-md-4 col-sm-4 border-info border-right">
                    <span><h4>Kategorie:</h4></span>
                    <hr/>
                    <span class="px-2"><a>Wszystkie</a></span><br>
                    
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 px-3 py-3">
                    <div id="Fquest" class="border-bottom border-dark small-rad pt-3 pb-1 mr-3" >
                        <div class="float-left"><h4>Pytanie testowe</h4>
                        <h6>Kat: </h6>Inne | <h6>Kiedy moje konto zostało utworzone ?</h6>
                        </div>
                        <div class="float-right align-middle"><button class="small-rad btn btn-info btn-md my-2 mr-5">Odpowiedz</button></div>
                        <div class="clearfix"></div>
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
        </div>

        <?php require_once 'common/end.php'; ?>
    </body>
</html>

